#!/bin/bash

hg clone -b 3.8 http://hg.tryton.org/trytond

modules=('account' 'account_asset' 'account_be' 'account_credit_limit' 'account_de_skr03' 'account_deposit' 'account_dunning' \
 	 'account_dunning_letter' 'account_fr' 'account_invoice' 'account_invoice_history' 'account_invoice_line_standalone' \
	 'account_invoice_stock' 'account_payment' 'account_payment_clearing' 'account_payment_sepa' 'account_payment_sepa_cfonb' \
	 'account_product' 'account_statement' 'account_stock_anglo_saxon' 'account_stock_continental' 'account_stock_landed_cost' \
	 'account_stock_landed_cost_weight' 'account_tax_rule_country' 'analytic_account' 'analytic_invoice' 'analytic_purchase' \
	 'analytic_sale' 'bank' 'calendar' 'calendar_classification' 'calendar_scheduling' 'calendar_todo' 'carrier' 'carrier_percentage' \
	 'carrier_weight' 'commission' 'commission_waiting' 'company' 'company_work_time' 'country' 'currency' 'customs' 'dashboard' \
	 'google_maps' 'ldap_authentication' 'ldap_connection' 'party' 'party_relationship' 'party_siret' 'party_vcarddav' 'product' \
	 'product_attribute' 'product_cost_fifo' 'product_cost_history' 'product_measurements' 'product_price_list' 'production' \
	 'project' 'project_invoice' 'project_plan' 'project_revenue' 'purchase' 'purchase_invoice_line_standalone' 'purchase_shipment_cost' \
	 'sale' 'sale_complaint' 'sale_credit_limit' 'sale_extra' 'sale_invoice_grouping' 'sale_opportunity' 'sale_price_list' \
	 'sale_promotion' 'sale_shipment_cost' 'sale_shipment_grouping' 'sale_stock_quantity' 'sale_supply' 'sale_supply_drop_shipment' \
	 'stock' 'stock_forecast' 'stock_inventory_location' 'stock_location_sequence' 'stock_lot' 'stock_lot_sled' 'stock_package' \
	 'stock_product_location' 'stock_split' 'stock_supply' 'stock_supply_day' 'stock_supply_forecast' 'stock_supply_production' \
	 'timesheet' 'timesheet_cost')

mkdir -p modules && cd modules
mkdir -p tryton && cd tryton

for i in "${modules[@]}"
do
   :
   hg clone -b 3.8 http://hg.tryton.org/modules/$i 
done

cd ..
 

trytonspain=('sale_discount' 'account_invoice_discount' 'stock_kit' 'sale_kit' 'product_kit' 'account_payment_sepa_es' \
             'account_jasper_reports' 'account_payment_days' 'account_parent_code' 'account_financial_statement' \
             'account_bank_statement' 'doc' 'company_bank' 'account_code_digits' 'aeat_347' 'account_payment_type' \
             'aeat_340_es' 'aeat_340' 'account_bank' 'aeat_349_es' 'aeat_303_es' 'aeat_303' 'aeat_349'\ 
             'account_move_party_required' 'account_invoice_consecutive' 'bank_validation' 'party_bank' 'company_party_bank' \
             'party_bank_es' 'party_bank_validation' 'account_treasury' 'doc_git' 'sale_invoice_cancel' \
             'purchase_invoice_cancel' 'company_logo' 'bank_es' 'account_statement_of_account' 'account_search_with_dot' \
             'account_payment_wizard' 'account_payment_forecast' 'account_payment_es_csb_58' \
             'account_payment_es_csb_34_1_la_caixa' 'account_payment_es_csb_34_11' 'account_payment_es_csb_34_01' \
             'account_payment_es_csb_34' 'account_payment_es_csb_32' 'account_payment_es_csb_19' 'account_payment' \
             'account_invoice_taxes_required' 'account_invoice_prevent_duplicates' 'account_es_pyme' 'account_es_ca' \
             'account_es' 'account_cash_management' 'account_bank_statement_counterpart' 'account_bank_statement_account' \
             'account_bank_statement_payment' 'account_reconcile' 'account_financial_statement_es' 'country_zip' \
             'account_invoice_price_list' 'sale_invoice_line_standalone' 'party_communication' 'account_payment_sepa' \
             'account_payment_type_move' 'account_payment_es' 'mass_editing' 'party_comment' 'ir_module_info' \
             'account_bank_statement_es_csb43' 'account_reconcile_different_party' 'account_payment_bank' \
             'sale_payment_type' 'sale_carrier' 'purchase_payment_type' 'party_vat_unique' 'party_tradename' 'party_search' \
             'jasper_reports' 'country_zip_es' 'account_invoice_posted2draft')
 
mkdir -p trytonspain && cd trytonspain

for i in "${trytonspain[@]}"
do
   :
   hg clone https://bitbucket.org/trytonspain/trytond-$i $i
done

cd ..

zz=('sale_pos' 'sale_discount_visible' 'stock_inventory_qty' 'sale_salesman' 'party_type' 'account_budget' 'stock_scanner' \
    'purchase_request_celery' 'product_configuration' 'product_variant_delete' 'product_sale_sequence' \
    'product_multiple_uom' 'stock_shipment_weight' 'stock_valued' 'product_template_code' 'account_invoice_origin_reference' \
    'product_purchase_sequence' 'stock_scanner_lot' 'product_attachments' 'stock_lot_quantity' \
    'stock_delivery_note_valued_jreport' 'stock_delivery_note_lot_jreport' 'stock_delivery_note_jreport' 'sale_jreport' \
    'purchase_jreport' 'account_invoice_jreport' 'sale_pos_discount' 'sale_opportunity_next_action' 'contract_invoice' \
    'project_timesheet' 'product_price_list_manufacturer' 'product_price_list_category' 'product_pack' 'esale' \
    'galatea_esale' 'stock_lot_quantity_location' 'project_helpdesk' 'helpdesk' 'galatea_blog' 'babi' 'product_esale' \
    'galatea' 'product_variant' 'sale_cart' 'patchs' 'product_barcode' 'product_manufacturer' 'galatea_cms' 'sale_shop' \
    'party_event' 'stock_shipment_return_price' 'stock_shipment_deviation' 'stock_origin' 'stock_move_warehouse' \
    'stock_lot_inventory_qty' 'stock_inventory_reference' 'sale_shipment_returns' 'account_dunning_mail' \
    'account_dunning_cron' 'sale_payment' 'prestashop_product' 'purchase_total' 'sale_total' 'stock_lot_out_autoassign' \
    'sale_invoice_grouping_by_address' 'carrier_send_shipments_seurvalencia' 'carrier_send_shipments_seur' \
    'carrier_send_shipments_mrw' 'carrier_send_shipments_envialia' 'carrier_send_shipments' 'purchase_delivery_date_manual' \
    'sale_delivery_date_manual' 'prestashop' 'sale_pos_jreport' 'electronic_mail_template' 'magento' 'electronic_mail' \
    'electronic_mail_wizard' 'account_invoice_sale_relation' 'sale_margin' 'default_value' 'base_external_mapping' \
    'magento_product' 'sale_shop_logo' 'sale_shipment_comment' 'pyme' 'account_invoice_merger' 'smtp' 'magento_stock' \
    'esale_stock' 'esale_product' 'stock_cart' 'sale_project_template' 'product_oneclick' 'sale_opportunity_helpdesk' \
    'carrier_formula' 'product_warranty' 'product_search_code' 'product_price_by_list_price' 'product_special_price' \
    'internetdomain' 'stock_location_warehouse' 'timetracker' 'stock_supply_request' 'stock_lot_sequence' \
    'stock_lot_supplier_ref' 'asterisk' 'activity_calendar' 'babi_reports_account' 'babi_reports_account_invoice' \
    'babi_reports_analytic_account' 'babi_reports_product' 'babi_reports_sale_opportunity' 'babi_reports_stock' \
    'file_format' 'project_template' 'purchase_lot_cost' 'stock_lot_expiry' 'stock_split_lot_expiry' 'purchase_contract' \
    'prestashop_stock' 'esale_sale' 'project_invoice_standalone' 'stock_shipment_return' 'magento_weight' \
    'magento_manufacturer' 'activity' 'sale_opportunity_activity' 'sale_asterisk' 'party_asterisk' \
    'account_asset_show_lines' 'csv_import' 'product_cost_plan_margin' 'stock_shipment_cashondelivery' \
    'sale_price_list_change_party' 'sale_payment_policy' 'sale_basket' 'getmail' 'carrier_zip' 'stock_lot_cost' \
    'sale_pos_esc' 'sale_opportunity_quote' 'carrier_file' 'subscription' 'stock_supply_supplier_description' \
    'stock_supply_best_supplier' 'stock_relation' 'stock_product_location_package' 'stock_origin_sale' \
    'stock_lot_shipment_return' 'stock_lot_out_autoassign_expiry' 'stock_lot_date' 'stock_helpdesk' 'stock_delivery_date' \
    'stock_delivery' 'stock_comment' 'searching' 'sale_weight' 'sale_waiting' 'sale_shipments_done' \
    'sale_shipment_cost_prices' 'sale_product_category_exclude' 'sale_pos_shipment_cost' 'sale_pos_salesman' \
    'sale_payment_web' 'sale_opportunity_talk' 'sale_opportunity_shop' 'sale_opportunity_converted' 'sale_min_qty' \
    'sale_margin_waiting' 'sale_invoices_paid' 'sale_invisible_stock' 'sale_helpdesk' 'sale_external_price' \
    'sale_data_shop' 'sale_data' 'sale_customer_reference' 'sale_confirmed2quotation' 'sale_commission' 'project_waiting' \
    'project_state_by_buttons' 'project_invoice_description' 'project_employee' 'product_sequence' \
    'product_price_list_formula' 'product_barcode_label' 'party_social_contact' 'party_salesman' 'party_mercantil' \
    'party_birthday' 'party_attribute' 'party_address_position' 'network' 'jasper_reports_options' \
    'internetdomain_invoice_standalone' 'internetdomain_invoice' 'internetdomain_alert' 'electronic_mail_event' 'django' \
    'dbcopy' 'csv_sale_shipment_cost' 'csv_sale' 'csv_purchase' 'csv_import_send_mail' 'csv_import_getmail' \
    'contract_formula' 'contract' 'carrier_service' 'carrier_payment_type' 'carrier_code' 'carrier_api' \
    'account_payment_csv' 'account_invoice_total' 'account_invoice_line_stand2invoice' 'account_invoice_helpdesk' \
    'account_invoice_data' 'product_cost_plan_operation' 'nereid_stock' 'nereid_smtp' 'nereid_sale' 'nereid_contact' \
    'nereid_cms' 'nereid_blog' 'nereid_basket' 'nereid_account_invoice' 'nereid' 'stock_valued_report' \
    'electronic_mail_pyme' 'stock_valued_discount' 'sale_opportunity_mail')

mkdir -p zikzakmedia && cd zikzakmedia

for i in "${zz[@]}"
do
   :
   hg clone https://bitbucket.org/zikzakmedia/trytond-$i $i
done

cd ..

nantic=('electronic_mail' 'electronic_mail_wizard' 'electronic_mail_template' 'electronic_mail_imap' 'imap' \
        'electronic_mail_activity' 'electronic_mail_model' 'sale_opportunity_campaign' 'sale_opportunity_activity' \
        'sale_opportunity' 'account_invoice_milestone' 'project_taskjuggler' 'company_account_sync' \
        'account_financial_statement_analytic' 'account_invoice_shop' 'patches' 'product_raw_variant' \
        'purchase_stock_account_move' 'quality_control' 'activity' 'sale_opportunity_campaign_contact' \
        'stock_external_party' 'sale_invoice_complete_line_grouping' 'farm_prescription' 'sale_product_package' \
        'purchase_product_package' 'farm_nutrition_program' 'farm' 'stock_lot_sequence' 'sale_product_raw' \
        'stock_lot_manufacturer' 'product_configuration' 'party_manufacturer' 'stock_scanner_lot' 'stock_scanner' \
        'purchase_information_uom' 'account_invoice_information_uom' 'stock_lot_cost' 'purchase_contract' \
        'sale_stock_account_move' 'production_quality_control' 'stock_lot_expiry' 'analytic_product' \
        'stock_supply_minimum' 'stock_supply_multiple' 'analytic_stock' 'sale_project_template' 'farm_breeding' \
        'timetracker' 'stock_lot_fifo' 'stock_external_lot' 'analytic_line_state' 'asterisk' 'quality_control_formula' \
        'project_priority' 'project_codereview' 'network_monitoring' 'network' 'stock_lot_quantity' \
        'purchase_supplier_price_period' 'party_relationship_department' 'party_relationship_position' \
        'stock_external_reception' 'stock_external' 'product_end_of_life' 'party_credit_limit' 'purchase_supplier_discount' \
        'stock_reservation' 'account_invoice_party_currency' 'sale_goal_natural_year' 'sale_goal' 'sale_salesman_party' \
        'production_process' 'product_package' 'sale_minimum' 'holidays_employee' 'product_cost_plan_operation' \
        'product_cost_plan' 'sale_customer_product' 'project_tracker' 'activity_rating' 'sale_opportunity_category' \
        'sale_opportunity_priority' 'account_invoice_contact' 'sale_opportunity_phase' 'product_variant_unique' \
        'quality_control_trigger' 'production_external' 'party_sector' 'sale_cancel_reason' 'getmail' \
        'production_operation' 'sale_farm' 'project_task_invoice' 'stock_move_label' 'account_invoice_conformity' \
        'analytic_asset' 'demo_base' 'project_phase' 'project_component' 'sale_pos_template_quantities' \
        'account_payment_receipt' 'sale_lot' 'project_helpdesk' 'production_supply_request' 'stock_supply_warehouse' \
        'stock_supply_production_warehouse' 'stock_shipment_edit_inline' 'stock_serial_number_file_import' \
        'stock_serial_number' 'stock_scanner_serial_number' 'stock_report_single' \
        'stock_quality_control_trigger_shipment_in' 'stock_quality_control_trigger_lot' 'stock_partial_shipment_out' \
        'stock_location_warehouse' 'stock_valued' 'stock_supply_request' 'stock_split_lot_expiry' 'sale_shop_trade_info' \
        'sale_pos_template_extra_products' 'sale_opportunity_contact' 'sale_line_address' 'stock_lot_supplier_ref' \
        'sale_issue' 'sale_invoice_period' 'sale_invoice_complete' 'sale_from_openerp' 'sale_fedicom' 'sale_delivery_date' \
        'sale_customer_product_restrict' 'sale_cost_plan' 'quality_control_sample' 'purchase_lot_cost' 'project_template' \
        'purchase_report_single' 'purchase_lot_expiry' 'file_format' 'babi_reports_stock' 'babi_reports_sale_opportunity' \
        'babi_reports_product' 'purchase_from_openerp' 'babi_reports_analytic_account' 'project_warranty' \
        'babi_reports_account_invoice' 'babi_reports_account' 'project_sequence' 'project_invoice_hours' 'project_expenses' \
        'project_contact' 'project_auto_timesheet' 'project_allocation_report' 'project_allocation_edit_inline' \
        'project_activity' 'account_budget' 'production_split_lot_expiry' 'production_split' 'production_lot_cost' \
        'production_efficiency_percentage' 'production_editable_tree' 'production_bom_versions' \
        'product_template_form_quantity' 'frepple' 'production_route' 'product_cost_plan_process' 'production_package' \
        'production_output_lot' 'production_origin' 'production_operation_tracking' 'product_cost_plan_margin' \
        'product_purchase_history' 'product_price_list_price_category' 'product_images' 'product_brand' 'party_asterisk' \
        'carrier_file' 'activity_contact' 'activity_calendar' 'account_payment_type_cost' 'account_invoice_sale_relation' \
        'account_invoice_line_search_by_shipment' 'account_asset_show_lines' 'account_asset_project' 'sale_contact' \
        'purchase_contact' 'stock_package' 'production_quality_control_trigger_lot' 'party_relationship' \
        'farm_nutrition_program_supply_request' 'farm_feed_production' 'account_invoice_stock' 'sale_asterisk' \
        'sale_opportunity_asterisk' 'contract_invoice_project' 'sale_pos_esc' 'sale_opportunity_quote')

mkdir -p nantic && cd nantic

for i in "${nantic[@]}"
do
   :
   hg clone https://bitbucket.org/nantic/trytond-$i $i
done

cd ../..

cp trytond/etc/trytond.conf .

cd trytond/trytond/modules/
ln -sf ../../../modules/tryton/* .
ln -sf ../../../modules/trytonspain/* .
cd ../../..
mkdir -p var
